#!/bin/sh
# Copyright © 2021 Luca Lovisa <opensource@void.li>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.
# SPDX-License-Identifier: WTFPL
#
set -e -x

# ----------

docker_host="registry.gitlab.com"
[ -n "$docker_host" ]

docker_path="void-li/alpina"
[ -n "$docker_path" ]

# ----------

if [ -n "$GITLAB_CI" ]; then
  ({ set +x; } 2>/dev/null; echo "$CI_BUILD_TOKEN") | \
    docker login -u gitlab-ci-token --password-stdin "$docker_host"
fi

dimage_name="$1"
[ -d "$dimage_name" ]

dimage_path="${docker_host%/}/${docker_path%/}/${dimage_name%/}"
[ -n "$dimage_path" ]

if [ -z "$GITLAB_CI" ]; then
  if [ -n "$(docker images -q "$dimage_path")" ]; then
    docker rmi "$dimage_path"
  fi
fi

docker build -t "$dimage_path" "$dimage_name"

if [ -n "$GITLAB_CI" ]; then
  docker push "$dimage_path:latest"
fi
